/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2011-2014 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

/* depo3d.cpp:
 * In this example the fluid flow through a bifurcation is
 * simulated. The geometry is obtained from a mesh in stl-format.
 * With Bouzidi boundary conditions the curved boundary is
 * adequately mapped and initialized fully automatically. As
 * dynamics a Smagorinsky turbulent BGK model is used to stabilize
 * the simulation for low resolutions. As output the flux at the
 * inflow and outflow region is computed. The results has been
 * validated by comparison with other results obtained with FEM
 * and FVM.
 */


#include "olb3D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb3D.hh"   // include full template code;
#endif
#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::util;
using namespace std;
typedef double T;
#define DESCRIPTOR D3Q19Descriptor


//simulation parameters
int N = 1000;             // resolution of the model //40
int M = 8;             // time discretization refinement
const bool bouzidiOn = true; // choice of boundary condition
const T maxPhysT = 0.05;       // max. simulation time in s, SI unit

 const double smag = 0.1;
 //POZOR BGK IS ON!


// Stores data from stl file in geometry in form of material numbers
void prepareGeometry( UnitConverter<T,DESCRIPTOR> const& converter, IndicatorF3D<T>& indicator,
                      STLreader<T>& stlReader, SuperGeometry3D<T>& superGeometry ) {

  OstreamManager clout( std::cout,"prepareGeometry" );//muzu pouzivat clout misto cout ma marker
  clout << "Prepare Geometry ..." << std::endl;

  superGeometry.rename( 0,2,indicator );
  superGeometry.rename( 2,1,stlReader );

  //superGeometry.rename( 2,3,1,stlReaderI1);
  //superGeometry.rename( 2,4,stlReaderO1);//stl reader sikminy to nedela dobre radsi specifikovat centrum atd.
  //superGeometry.rename( 2,5,1,stlReaderO2);
  superGeometry.clean();
 // clout << "VUT Brno TEST 74" << std::endl; 
  std::cout << "ConversionFactorLength: " << converter.getConversionFactorLength() << std::endl;
  
  // center of inflow and outflow regions [m] //CCM
  Vector<T, 3> Center_1_inlet(0.00000,0.00000,0.00000);//kontrolovano v Paraview, zda se byti v pohode
  Vector<T, 3> Center_23_outlet(0.07954,0.14483,-0.16297);
  Vector<T, 3> Center_24_outlet(0.02338,0.19556,-0.24493);
  Vector<T, 3> Center_25_outlet(0.07373,0.13761,-0.30437);
  Vector<T, 3> Center_26_outlet(0.10784,0.13985,-0.23444);
  Vector<T, 3> Center_27_outlet(-0.10248,0.04263,-0.25454);
  Vector<T, 3> Center_28_outlet(-0.04939,0.10640,-0.30456);
  Vector<T, 3> Center_29_outlet(-0.10272,0.10355,-0.16101);
  Vector<T, 3> Center_30_outlet(-0.09756,0.11193,-0.23580);
  Vector<T, 3> Center_31_outlet(-0.07001,0.18906,-0.18363);
  Vector<T, 3> Center_32_outlet(-0.05100,0.17800,-0.24917);

  // radii of inflow and outflow regions [m] //CCM
  T inletRadius = 0.01;//CCM //0.0075//puvodne 0.01
  T outletRadius = 0.005;//CCM //0.005//puvodne 0.01

  // normals of inflow and outflow regions //CCM
  Vector<T, 3> Normal_1_inlet(0.00058,-1.00000,0.00000);//kontrolovano v Paraview, zda se byti v pohode
  Vector<T, 3> Normal_23_outlet(0.63068,0.35386,0.69067);
  Vector<T, 3> Normal_24_outlet(-0.16004,0.95676,-0.24288);
  Vector<T, 3> Normal_25_outlet(-0.42753,-0.15310,0.89094);
  Vector<T, 3> Normal_26_outlet(0.83642,0.45703,-0.30252);
  Vector<T, 3> Normal_27_outlet(0.61275,0.66694,0.42394);
  Vector<T, 3> Normal_28_outlet(-0.16534,-0.02538,-0.98591);
  Vector<T, 3> Normal_29_outlet(0.85719,0.15894,-0.48987);
  Vector<T, 3> Normal_30_outlet(0.93077,-0.33507,0.14628);
  Vector<T, 3> Normal_31_outlet(0.49895,-0.85748,-0.12562);
  Vector<T, 3> Normal_32_outlet(-0.21309,0.93427,-0.28590);

  IndicatorCircle3D<T> inletCircle(Center_1_inlet,Normal_1_inlet,0.95 *inletRadius);
  IndicatorCylinder3D<T> inlet1( inletCircle,2 * converter.getConversionFactorLength() );
// clout << "VUT Brno TEST 77" << std::endl;
// clout << "Vykresli v Rhinu pro kontrolu IndicatorCylinder3D zdali predepisuje spravne layerInflow aby nebyla chyba v superGeometry.rename( 2,3,1,layerInflow ) "  << endl;//DEL

  // rename the material at the outlet0,outlet1
  IndicatorCircle3D<T> outletCircle23(Center_23_outlet,Normal_23_outlet,0.95 * outletRadius);IndicatorCylinder3D<T> outlet23( outletCircle23,8 * converter.getConversionFactorLength() );
  IndicatorCircle3D<T> outletCircle24(Center_24_outlet,Normal_24_outlet,0.95 * outletRadius);IndicatorCylinder3D<T> outlet24( outletCircle24,8 * converter.getConversionFactorLength() );
  IndicatorCircle3D<T> outletCircle25(Center_25_outlet,Normal_25_outlet,0.95 * outletRadius);IndicatorCylinder3D<T> outlet25( outletCircle25,8 * converter.getConversionFactorLength() );
  IndicatorCircle3D<T> outletCircle26(Center_26_outlet,Normal_26_outlet,0.95 * outletRadius);IndicatorCylinder3D<T> outlet26( outletCircle26,8 * converter.getConversionFactorLength() );
  IndicatorCircle3D<T> outletCircle27(Center_27_outlet,Normal_27_outlet,0.8 * outletRadius);IndicatorCylinder3D<T> outlet27( outletCircle27,8 * converter.getConversionFactorLength() );
  IndicatorCircle3D<T> outletCircle28(Center_28_outlet,Normal_28_outlet,0.95 * outletRadius);IndicatorCylinder3D<T> outlet28( outletCircle28,8 * converter.getConversionFactorLength() );
  IndicatorCircle3D<T> outletCircle29(Center_29_outlet,Normal_29_outlet,0.95 * outletRadius);IndicatorCylinder3D<T> outlet29( outletCircle29,8 * converter.getConversionFactorLength() );
  IndicatorCircle3D<T> outletCircle30(Center_30_outlet,Normal_30_outlet,0.75 * outletRadius);IndicatorCylinder3D<T> outlet30( outletCircle30,8 * converter.getConversionFactorLength() );
  IndicatorCircle3D<T> outletCircle31(Center_31_outlet,Normal_31_outlet,0.95 * outletRadius);IndicatorCylinder3D<T> outlet31( outletCircle31,8 * converter.getConversionFactorLength() );
  IndicatorCircle3D<T> outletCircle32(Center_32_outlet,Normal_32_outlet,0.95 * outletRadius);IndicatorCylinder3D<T> outlet32( outletCircle32,8 * converter.getConversionFactorLength() );
 
//  clout << "VUT Brno TEST 89" << std::endl;
  superGeometry.rename( 2,3,inlet1);
  superGeometry.rename( 2,4,outlet23);//COMM //doplneny jednicky
  superGeometry.rename( 2,4,outlet24);//COMM 
  superGeometry.rename( 2,4,outlet25);//COMM 
  superGeometry.rename( 2,4,outlet26);//COMM 
  superGeometry.rename( 2,5,outlet27);//COMM 
  superGeometry.rename( 2,5,outlet28);//COMM 
  superGeometry.rename( 2,5,outlet29);//COMM 
  superGeometry.rename( 2,5,outlet30);//COMM 
  superGeometry.rename( 2,5,outlet31);//COMM 
  superGeometry.rename( 2,5,outlet32);//COMM 
 
//  clout << "VUT Brno TEST 91" << std::endl;  
  // Removes all not needed boundary voxels outside the surface
  superGeometry.clean();
  // Removes all not needed boundary voxels inside the surface
 // superGeometry.innerClean( true );
  //superGeometry.innerClean( 4 );
	//superGeometry.innerClean( 5 );
	//superGeometry.outerClean();
  superGeometry.checkForErrors();
 // clout << "VUT Brno TEST 97" << std::endl;
  superGeometry.print();
  clout << "Prepare Geometry ... OK" << std::endl;
}

// Set up the geometry of the simulation
void prepareLattice( SuperLattice3D<T, DESCRIPTOR>& lattice,
                     UnitConverter<T,DESCRIPTOR> const& converter,
					 Dynamics<T, DESCRIPTOR>& bulkDynamics,
                     sOnLatticeBoundaryCondition3D<T, DESCRIPTOR>& bc,
                     sOffLatticeBoundaryCondition3D<T,DESCRIPTOR>& offBc,
                     STLreader<T>& stlReader, SuperGeometry3D<T>& superGeometry ) 
					 {

  OstreamManager clout( std::cout,"prepareLattice" );
  clout << "Prepare Lattice ..." << std::endl;
 // clout << "VUT Brno TEST 111" << std::endl;
  const T omega = converter.getLatticeRelaxationFrequency();

  // material=0 --> do nothing
  lattice.defineDynamics( superGeometry,0,&instances::getNoDynamics<T, DESCRIPTOR>() );

  // material=1 --> bulk dynamics
  lattice.defineDynamics( superGeometry,1,&bulkDynamics );

  if ( bouzidiOn ) {
    // material=2 --> no dynamics + bouzidi zero velocity
    lattice.defineDynamics( superGeometry,2,&instances::getNoDynamics<T,DESCRIPTOR>() );
    offBc.addZeroVelocityBoundary( superGeometry,2,stlReader );
    // material=3 --> no dynamics + bouzidi velocity (inflow)
    lattice.defineDynamics( superGeometry,3,&instances::getNoDynamics<T,DESCRIPTOR>() );
    offBc.addVelocityBoundary( superGeometry,3,stlReader );
  } else {
    // material=2 --> bounceBack dynamics
    lattice.defineDynamics( superGeometry, 2, &instances::getBounceBack<T, DESCRIPTOR>() );
    // material=3 --> bulk dynamics + velocity (inflow)
    lattice.defineDynamics( superGeometry,3,&bulkDynamics );
    bc.addVelocityBoundary( superGeometry,3,omega );
  }

  // material=4,5 --> bulk dynamics + pressure (outflow)
  lattice.defineDynamics( superGeometry,4,&bulkDynamics );
  lattice.defineDynamics( superGeometry,5,&bulkDynamics );
  bc.addPressureBoundary( superGeometry,4,omega );
  bc.addPressureBoundary( superGeometry,5,omega );
 // clout << "VUT Brno TEST 140" << std::endl;
  // Initial conditions
  AnalyticalConst3D<T,T> rhoF( 1 );
  std::vector<T> velocity( 3,T() );
  AnalyticalConst3D<T,T> uF( velocity );

  // Initialize all values of distribution functions to their local equilibrium
  lattice.defineRhoU( superGeometry,1,rhoF,uF );
  lattice.iniEquilibrium( superGeometry,1,rhoF,uF );
  lattice.defineRhoU( superGeometry,3,rhoF,uF );
  lattice.iniEquilibrium( superGeometry,3,rhoF,uF );
  lattice.defineRhoU( superGeometry,4,rhoF,uF );
  lattice.iniEquilibrium( superGeometry,4,rhoF,uF );
  lattice.defineRhoU( superGeometry,5,rhoF,uF );
  lattice.iniEquilibrium( superGeometry,5,rhoF,uF );

  // Lattice initialize
  lattice.initialize();

  clout << "Prepare Lattice ... OK" << std::endl;
}

// Generates a slowly increasing sinuidal inflow
void setBoundaryValues( SuperLattice3D<T, DESCRIPTOR>& sLattice,
                        sOffLatticeBoundaryCondition3D<T,DESCRIPTOR>& offBc,
                        UnitConverter<T,DESCRIPTOR> const& converter, int iT,
                        SuperGeometry3D<T>& superGeometry ) {
							
  // No of time steps for smooth start-up
  int iTperiod = converter.getLatticeTime( 1. );
  int iTupdate = 50;

  if ( iT%iTupdate == 0 ) {
    // Smooth start curve, sinus
    SinusStartScale<T,int> nSinusStartScale( iTperiod,converter.getCharLatticeVelocity() );

    // Creates and sets the Poiseuille inflow profile using functors
    int iTvec[1]= {iT};
    T maxVelocity[1]= {T()};
    nSinusStartScale( maxVelocity,iTvec );
	if(iT >converter.getLatticeTime(1.))
	{
		CirclePoiseuille3D<T> velocity( superGeometry,3,-maxVelocity[0] );
		if ( bouzidiOn ) {
      offBc.defineU( superGeometry,3,velocity );
    } else {
      sLattice.defineU( superGeometry,3,velocity );
    }
	}
		
	else
	{
	CirclePoiseuille3D<T> velocity( superGeometry,3,maxVelocity[0] );

    if ( bouzidiOn ) {
      offBc.defineU( superGeometry,3,velocity );
    } else {
      sLattice.defineU( superGeometry,3,velocity );
    }
	}
  }
}

// Computes flux at inflow and outflow
void getResults( SuperLattice3D<T, DESCRIPTOR>& sLattice,
                 UnitConverter<T,DESCRIPTOR>& converter, int iT,
                 Dynamics<T, DESCRIPTOR>& bulkDynamics,
                 SuperGeometry3D<T>& superGeometry, Timer<T>& timer, STLreader<T>& stlReader) {

  OstreamManager clout( std::cout,"getResults" );

  SuperVTMwriter3D<T> vtmWriter( "depo3d" );
  SuperLatticePhysVelocity3D<T, DESCRIPTOR> velocity( sLattice, converter );
  SuperLatticePhysPressure3D<T, DESCRIPTOR> pressure( sLattice, converter );
  vtmWriter.addFunctor( velocity );
  vtmWriter.addFunctor( pressure );
  const int vtkIter  = converter.getLatticeTime( .025 );
	const int statIter = converter.getLatticeTime( .001 );


  if ( iT==0 ) {
    // Writes the geometry, cuboid no. and rank no. as vti file for visualization
    SuperLatticeGeometry3D<T, DESCRIPTOR> geometry( sLattice, superGeometry );
    SuperLatticeCuboid3D<T, DESCRIPTOR> cuboid( sLattice );
    SuperLatticeRank3D<T, DESCRIPTOR> rank( sLattice );
    vtmWriter.write( geometry );
    vtmWriter.write( cuboid );
    vtmWriter.write( rank );

    vtmWriter.createMasterFile();
  }

  // Writes the vtk files
  if ( iT%vtkIter==0 ) {
    vtmWriter.write( iT );

    SuperEuklidNorm3D<T, DESCRIPTOR> normVel( velocity );
    BlockReduction3D2D<T> planeReduction( normVel, {0,0,1}, 600, BlockDataSyncMode::ReduceOnly );
    // write output as JPEG
    heatmap::write(planeReduction, iT);
  }

  // Writes output on the console
  if ( iT%statIter==0 ) {
    // Timer console output
    timer.update( iT );
    timer.printStep();

    // Lattice statistics console output
    sLattice.getStatistics().print( iT,converter.getPhysTime( iT ) );

    // Flux at the inflow and outflow region
    std::vector<int> materials = { 1, 3, 4, 5 };
 
  /*
    //IndicatorCircle3D<T> inflow(-0.003398273,0.09795340+2.*converter.getConversionFactorLength(),-0.08125871,0,0,-1,0.0075+2.*converter.getConversionFactorLength());
	IndicatorCircle3D<T> inflow(-0.003398273,0.09795340,-0.08125871,0,0,-1,0.0075);
    SuperPlaneIntegralFluxVelocity3D<T> vFluxInflow( sLattice, converter, superGeometry, inflow, materials, BlockDataReductionMode::Discrete );
    vFluxInflow.print( "inflow","ml/s" );
    SuperPlaneIntegralFluxPressure3D<T> pFluxInflow( sLattice, converter, superGeometry, inflow, materials, BlockDataReductionMode::Discrete );
    pFluxInflow.print( "inflow","N","mmHg" );


    //IndicatorCircle3D<T> outflow0(0.03394469,0.09795340+2.*converter.getConversionFactorLength(),-0.2601402, 0.7565347,0.03118337,-0.6532096,0.005+2.*converter.getConversionFactorLength());
    IndicatorCircle3D<T> outflow0(0.03394469,0.09795340+2.*converter.getConversionFactorLength(),-0.2601402, 0,0,1,0.005+2.*converter.getConversionFactorLength());
  
	SuperPlaneIntegralFluxVelocity3D<T> vFluxOutflow0( sLattice, converter, superGeometry, outflow0, materials, BlockDataReductionMode::Discrete );
    vFluxOutflow0.print( "outflow0","ml/s" );
    SuperPlaneIntegralFluxPressure3D<T> pFluxOutflow0( sLattice, converter, superGeometry, outflow0, materials, BlockDataReductionMode::Discrete );
    pFluxOutflow0.print( "outflow0","N","mmHg" );

   // IndicatorCircle3D<T> outflow1( -0.02203623,0.09795340+2.*converter.getConversionFactorLength(),-0.2457337,-0.7210884,0.01494763,-0.6926818,0.005+2.*converter.getConversionFactorLength());
     IndicatorCircle3D<T> outflow1( -0.02203623,0.09795340+2.*converter.getConversionFactorLength(),-0.2457337,0,0,1,0.005+2.*converter.getConversionFactorLength());
 	SuperPlaneIntegralFluxVelocity3D<T> vFluxOutflow1( sLattice, converter, superGeometry, outflow1, materials, BlockDataReductionMode::Discrete );
    vFluxOutflow1.print( "outflow1","ml/s" );
    SuperPlaneIntegralFluxPressure3D<T> pFluxOutflow1( sLattice, converter, superGeometry, outflow1, materials, BlockDataReductionMode::Discrete );
    pFluxOutflow1.print( "outflow1","N","mmHg" );
	*/

    if ( bouzidiOn ) {
      SuperLatticeYplus3D<T, DESCRIPTOR> yPlus( sLattice, converter, superGeometry, stlReader, 3 );
      SuperMax3D<T> yPlusMaxF( yPlus, superGeometry, 1 );
      int input[4]= {};
      T yPlusMax[1];
      yPlusMaxF( yPlusMax,input );
      clout << "yPlusMax=" << yPlusMax[0] << std::endl;
    }	
  }

  if ( sLattice.getStatistics().getMaxU() > 0.3 ) {
    clout << "PROBLEM uMax=" << sLattice.getStatistics().getMaxU() << std::endl;
    vtmWriter.write( iT );
    std::exit( 0 );
  }
}

int main( int argc, char* argv[] ) {
	
	//commander parameters
	//N --- spatial resolution
	//M --- temporal resolution
	//"./tmp/" output folder name
	std::string name;
	if(argc>2)
	{
	N = atoi(argv[1]);
	M = atoi(argv[2]);
	name = to_string("./tmp_N=")+to_string(argv[1])+to_string("_M=") + to_string(argv[2]) + to_string("/");
	}
	else
	name = "./tmp/";
	
  // === 1st Step: Initialization ===
  olbInit( &argc, &argv );
  singleton::directories().setOutputDir( name );
  OstreamManager clout( std::cout,"main" );
  // display messages from every single mpi process
  //clout.setMultiOutput(true);

  UnitConverter<T,DESCRIPTOR> converter(
    (T)   0.300/N,     // physDeltaX: spacing between two lattice cells in __m__
    (T)   0.3/(M*N), // physDeltaT: time step in __s__
    (T)   0.300,       // charPhysLength: reference length of simulation geometry
    (T)   0.1,          // charPhysVelocity: maximal/highest expected velocity during simulation in __m / s__
   // (T)   0.003/1055.,   // physViscosity: physical kinematic viscosity in __m^2 / s__
     (T)   0.0000185,   // physViscosity: physical kinematic viscosity in __m^2 / s__
    (T)   1.3          // physDensity: physical density in __kg / m^3__
  );
  // Prints the converter log as console output
  converter.print();
  // Writes the converter log in a file
  converter.write("depo3d");

  // === 2nd Step: Prepare Geometry ===
 // clout << "VUT Brno TEST 299" << std::endl;
  // Instantiation of the STLreader class
  // file name, voxel size in meter, stl unit in meter, outer voxel no., inner voxel no.

  //STLreader<T> stlReader( "W.stl", converter.getConversionFactorLength(), 1, 0, true );
  //STLreader<T> stlReaderI1( "I1.stl", converter.getConversionFactorLength(), 1, 0, true );
  //STLreader<T> stlReaderO1( "O1.stl", converter.getConversionFactorLength(), 1, 0, true);
  //STLreader<T> stlReaderO2( "O2.stl", converter.getConversionFactorLength(), 1, 0, true);
 // STLreader<T> stlReader( "Depo7gen_cfd.stl", converter.getConversionFactorLength(), 0.001, 0, true );
  //STLreader<T> stlReader( "plice_changed_mesh.stl", converter.getConversionFactorLength(), 1, 0, true );
	STLreader<T> stlReader( "plice_0.0005m.stl", converter.getConversionFactorLength(), 1, 0, true );
  //stlReaderO1.setNormalsOutside ();stlReaderO2.setNormalsOutside ();
  IndicatorLayer3D<T> extendedDomain( stlReader, converter.getConversionFactorLength() );

  // Instantiation of a cuboidGeometry with weights
#ifdef PARALLEL_MODE_MPI
  const int noOfCuboids = std::min( 16*N,4*singleton::mpi().getSize() );
#else
  const int noOfCuboids = 2;
#endif
  CuboidGeometry3D<T> cuboidGeometry( extendedDomain, converter.getConversionFactorLength(), noOfCuboids );
  //clout << "VUT Brno TEST 312" << std::endl;
  // Instantiation of a loadBalancer
  HeuristicLoadBalancer<T> loadBalancer( cuboidGeometry );

  // Instantiation of a superGeometry
  SuperGeometry3D<T> superGeometry( cuboidGeometry, loadBalancer, 2 );
 // clout << "VUT Brno TEST 318" << std::endl;
  prepareGeometry( converter, extendedDomain, stlReader,superGeometry );

  // === 3rd Step: Prepare Lattice ===
  SuperLattice3D<T, DESCRIPTOR> sLattice( superGeometry );

 // SmagorinskyBGKdynamics<T, DESCRIPTOR> bulkDynamics( converter.getLatticeRelaxationFrequency(),instances::getBulkMomenta<T, DESCRIPTOR>(), smag );
	   BGKdynamics<T, DESCRIPTOR> bulkDynamics (
    converter.getLatticeRelaxationFrequency(),
    instances::getBulkMomenta<T,DESCRIPTOR>()
  );

  // choose between local and non-local boundary condition
  sOnLatticeBoundaryCondition3D<T,DESCRIPTOR> sBoundaryCondition( sLattice );
  createInterpBoundaryCondition3D<T,DESCRIPTOR>( sBoundaryCondition );
  // createLocalBoundaryCondition3D<T,DESCRIPTOR>(sBoundaryCondition);
  //clout << "VUT Brno TEST 331" << std::endl;
  sOffLatticeBoundaryCondition3D<T, DESCRIPTOR> sOffBoundaryCondition( sLattice );
  createBouzidiBoundaryCondition3D<T, DESCRIPTOR> ( sOffBoundaryCondition );

  Timer<T> timer1( converter.getLatticeTime( maxPhysT ), superGeometry.getStatistics().getNvoxel() );
  timer1.start();

  prepareLattice( sLattice, converter, bulkDynamics,
                  sBoundaryCondition, sOffBoundaryCondition,
                  stlReader, superGeometry );

  timer1.stop();
  timer1.printSummary();
  //clout << "VUT Brno TEST 344" << std::endl;
  // === 4th Step: Main Loop with Timer ===
  clout << "starting simulation..." << std::endl;
  Timer<T> timer( converter.getLatticeTime( maxPhysT ), superGeometry.getStatistics().getNvoxel() );
  timer.start();

 //for ( int iT = 0; iT <=5; iT++ ) {
   for ( int iT = 0; iT <= converter.getLatticeTime(maxPhysT); iT++ ) {  //clout << "VUT Brno TEST 351" << std::endl;
    // === 5th Step: Definition of Initial and Boundary Conditions ===
  setBoundaryValues( sLattice, sOffBoundaryCondition, converter, iT, superGeometry );
 // clout << "VUT Brno TEST 354" << std::endl;
    // === 6th Step: Collide and Stream Execution ===
  sLattice.collideAndStream();
 // clout << "VUT Brno TEST 357" << std::endl;
    // === 7th Step: Computation and Output of the Results ===
 // clout << "VUT Brno TEST 399" << std::endl;
  getResults( sLattice, converter, iT, bulkDynamics, superGeometry, timer, stlReader);
  }
 // clout << "VUT Brno TEST 361" << std::endl;
  timer.stop();
  timer.printSummary();
}